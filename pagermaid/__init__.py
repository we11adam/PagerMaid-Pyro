import os

from shutil import copyfile
from coloredlogs import ColoredFormatter
from logging import getLogger, StreamHandler, CRITICAL, INFO, basicConfig
from datetime import datetime
from os import getcwd
from os import path as os_path
from apscheduler.schedulers.asyncio import AsyncIOScheduler

from pagermaid.config import Config
import pyromod.listen
from pyrogram import Client

# init folders
if not os_path.exists("data"):
    os.mkdir("data")
# init permissions
if not os_path.exists(f"data{os_path.sep}gm_policy.csv"):
    copyfile(f"{__path__[0]}{os_path.sep}assets{os_path.sep}gm_policy.csv", f"data{os_path.sep}gm_policy.csv")

CMD_LIST = {}
module_dir = __path__[0]
working_dir = getcwd()
# solve same process
read_context = {}
help_messages = {}
all_permissions = []
scheduler = AsyncIOScheduler()
if not scheduler.running:
    scheduler.configure(timezone="Asia/ShangHai")
    scheduler.start()
logs = getLogger(__name__)
logging_format = "%(levelname)s [%(asctime)s] [%(name)s] %(message)s"
logging_handler = StreamHandler()
logging_handler.setFormatter(ColoredFormatter(logging_format))
root_logger = getLogger()
root_logger.setLevel(CRITICAL)
root_logger.addHandler(logging_handler)
basicConfig(level=INFO)
logs.setLevel(INFO)

# easy check
if not Config.API_ID:
    logs.error("Api-ID Not Found!")
    exit(1)
elif not Config.API_HASH:
    logs.error("Api-Hash Not Found!")
    exit(1)

start_time = datetime.utcnow()
if Config.PROXY_ADDR and Config.PROXY_PORT:
    bot = Client(
        "PagerMaid",
        api_id=Config.API_ID,
        api_hash=Config.API_HASH,
        proxy=dict(
            hostname=Config.PROXY_ADDR,
            port=int(Config.PROXY_PORT)
        )
    )
else:
    bot = Client(
        "PagerMaid",
        api_id=Config.API_ID,
        api_hash=Config.API_HASH
    )


# bot = Client("pagermaid", api_id=Config.API_ID, api_hash=Config.API_HASH, ipv6=Config.IPV6)


async def log(message):
    logs.info(
        message.replace('`', '\"')
    )
    if not Config.LOG:
        return
    await bot.send_message(
        Config.LOG_ID,
        message
    )
